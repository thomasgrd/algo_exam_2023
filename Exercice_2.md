
Ecrire un algortithme qui demande à l'utilisateur de saisir un entier N et de lui afficher à l'écran une pyramide de hauteur N (comme le montre la figure ci-dessous pour N=10).

```
         *
        ***
       *****
      *******
     *********
    ***********
   *************
  ***************
 *****************
*******************
```

Réponse:

```python
# Demande à l'utilisateur de saisir un entier N
N = int(input("Saisir le nombre des lignes:"))

n_espace =  N - 1
n_etoile = 1

# Pour chaque etage du sapin
for i in range(0, N):
    # on affiche le bon nombre d'espace et d'etoile du sapin
    print (n_espace * " " + n_etoile * "*")
    # on met à jour les variables pour l'étage suivant
    n_espace = n_espace - 1 
    n_etoile = n_etoile + 2


```
Pour une meilleur visiualisation, voici 3 sapins de différentes tailles:

(~ : Représente ici un espace)

* ici N=4, espaces sur la 1ere ligne: 3
```
~~~*
~~***
~*****
*******
```

* ici N=3, espaces sur la 1ere ligne: 2
```
~~*
~***
***** 
```

* ici N=2, espaces sur la 1ere ligne: 1
```
~*
***
```