Soit le tas (T) affiché ci dessous.

![](image.png)

**Question 1:**
On souhaite représenter ce tas(T) par le tableau "Tab".
Afficher le contenu de Tab.

Réponse:

| 10 | 20 | 45 | 80 | 100 | 70 | 90 | 120 | 150 | 200 | 170 | 130 |


**Question 2:**
Ecrire la methode permettant de retourner le plus grand élément du tableau Tab.

Réponse:

```python
def max(tab):
    # On initialise notre valeur max actuelle
    current_max = tab[0]

    # parcourt tout le tableau
    for i in range(0, len(tab) - 1):
        # si on trouve une valeur supérieur à notre valeur max:
        # alors on met notre current_max à jour
        if tab[i] > current_max:
            current_max = tab[i]

    # A la fin        
    return current_max

```

**Question 3:**
Appliquer l'algorithme de Tri-fusion sur ce tableau afin d'obtenir un tableau trié par ordre croissant.

Réponse:
Schema du tri fusion

**Question 4:**

Appliquer l'algorithme "Mystère" sur le tableau Tab trié obtenu et déduire ce qu'il fait.

```
début
Algorithme Mystère
    endroit = 1;
    g=N;
    tant que  endroit < g faire
        milieu = (endroit + g) / 2;
        si L[milieu] < A alors
            endroit = milieu + 1;
        sinon g = milieu;
        finsi;
    fait;
fin

```

Pour aider:

Tableau Tab Trié obtenu précédemment:
| 10 | 20 | 45 | 70 | 80 | 90 | 100 | 120 | 130 | 150 | 170 | 200 |

Voici l'algo mystère en python (L: tableau trié, A: valeur à trouver dans le tableau trié):
```python
def mystere(L, A):
    endroit = 0
    g=len(tab) - 1
    while endroit < g:
        milieu = int((endroit + g) / 2)
        if L[milieu] < A:
            endroit = milieu + 1
        else:
            g = milieu
    return endroit
```
> En python, la position du tableau commence à 0 et en mode algo, la position commence à 1

Réponse:

Application de l'algo à notre tableau trié "L":

On test l'algo avec A = 170

```python
e = 1 (position du 1er element de notre tableau)
g = 12 (position du dernier element de notre tableau)

* 1ere itération de boucle:
e < g (donc on rentre dans la boucle while)
m = (1 + 12) / 2 = 6
if L[6] < A   (90) < 170)
    e = m + 1 = 6 + 1 = 7

* 2eme itération de boucle:
e < g (donc on refait un tour de boucle while)
m = (e + g) / 2 = (7 + 12) / 2 = 9
if L[9] < A (130 < 170>)
    e = m + 1 = 9 + 1 = 10

* 3eme itération de boucle:
e < g (donc on refait un tour de boucle while)
m = (e + g) / 2 = (10 + 12) / 2 = 11
if L[11] < A (170 < 170)
 # on va dans le else cette fois ci
else
    g = 11

* 4eme itération de boucle:
e < g (donc on refait un tour de boucle while)
m = 10 + 11 / 2 = 10
if L[10] < 170 (150 < 170)
    e = 10 + 1

* Arret de la boucle
e = g (11 = 11) (donc la boucle while s arrête et on retourne g qui vaut ici 11)
```
 
On appliquant l'algo avec notre tableau trié "L" et notre élément A "170", l'algo retourne à la fin 11 et L[11] vaut 170, donc l'algo a bien retourné la position de l'élément A dans le tableau trié an utilisant une méthode dicothomique.
